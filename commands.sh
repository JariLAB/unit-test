#! /bin/bash
mkdir -p ~/LAB/Ohjelmistojen ylläpito ja testaus/projects/stam/L03/unit-test
code ~/LAB/Ohjelmistojen ylläpito ja testaus/projects/stam/L03/unit-test

git init
npm init -y

npm install --save express
npm install --save-dev mocha chai

echo "node_modules" > .gitignore

# git remote add origin https://gitlab.com/JariLAB/unit-test
# git remote set-url origin ...
# git push -u origin main

touch README.md
mkdir src test
touch src/main.js src/calc.js
touch test/calc.test.js
mkdir public
touch public/index.html

# test files that goes to staging area
$ git add . --dry-run